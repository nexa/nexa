<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>Feu clic dret per a editar l&apos;adreça o l&apos;etiqueta</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Crea una nova adreça</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Nova</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Copia l&apos;adreça seleccionada al porta-retalls del sistema</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copia</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>&amp;Tanca</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;Copia l&apos;adreça</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Elimina l&apos;adreça sel·leccionada actualment de la llista</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exporta les dades de la pestanya actual a un fitxer</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;Exporta</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>&amp;Elimina</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Trieu una adreça on voleu enviar monedes</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Trieu l&apos;adreça on voleu rebre monedes</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>T&amp;ria</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>S&apos;estan enviant les adreces</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>S&apos;estan rebent les adreces</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Aquestes són les vostres adreces de Nexa per enviar els pagaments. Sempre reviseu l&apos;import i l&apos;adreça del destinatari abans de transferir monedes.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Aquestes són les vostres adreces Nexa per rebre pagaments. Es recomana utilitzar una adreça nova de recepció per a cada transacció.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>Copia l&apos;&amp;etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edita</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Exporta la llista d&apos;adreces</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Fitxer de separació amb comes (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>L&apos;exportació ha fallat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>S&apos;ha produït un error en desar la llista d&apos;adreces a %1. Torneu-ho a provar.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(sense etiqueta)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Diàleg de contrasenya</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Introduïu una contrasenya</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Nova contrasenya</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Repetiu la nova contrasenya</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>Encripta el moneder</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Aquesta operació requereix la contrasenya del moneder per a desbloquejar-lo.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Desbloqueja el moneder</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Aquesta operació requereix la contrasenya del moneder per desencriptar-lo.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Desencripta el moneder</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Canvia la contrasenya</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>Confirma l&apos;encriptació del moneder</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Avís: si encripteu el vostre moneder i perdeu la contrasenya, &lt;b&gt;PERDREU TOTS ELS VOSTRES NEXA COINS&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Esteu segur que voleu encriptar el vostre moneder?</translation>
    </message>
    <message>
        <location line="+119"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Avís: Les lletres majúscules estan activades!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Moneder encriptat</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Introduïu la contrasenya nova al moneder.&lt;br/&gt;Utilitzeu una contrasenya de &lt;b&gt;deu o més caràcters aleatoris&lt;/b&gt;, o &lt;b&gt;vuit o més paraules&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Introduïu la contrasenya antiga i la contrasenya nova al moneder.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>L&apos;encriptació del moneder ha fallat</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>L&apos;encriptació del moneder ha fallat per un error intern. El moneder no ha estat encriptat.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>La contrasenya introduïda no coincideix.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>El desbloqueig del moneder ha fallat</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>La contrasenya introduïda per a desencriptar el moneder és incorrecta.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>La desencriptació del moneder ha fallat</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>La contrasenya del moneder ha estat modificada correctament.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IP / Màscara de xarxa</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>Bandejat fins</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation type="unfinished">Agent d&apos;usuari</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+351"/>
        <source>Sign &amp;message...</source>
        <translation>Signa el &amp;missatge...</translation>
    </message>
    <message>
        <location line="+435"/>
        <source>Synchronizing with network...</source>
        <translation>S&apos;està sincronitzant amb la xarxa ...</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>&amp;Panorama general</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Node</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Mostra el panorama general del moneder</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transaccions</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Cerca a l&apos;historial de transaccions</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>S&amp;urt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Surt de l&apos;aplicació</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Quant a &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Mostra informació sobre Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Opcions...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>&amp;Encripta el moneder...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>&amp;Realitza una còpia de seguretat del moneder...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>&amp;Canvia la contrasenya...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>Adreces d&apos;e&amp;nviament...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>Adreces de &amp;recepció</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>Obre un &amp;URI...</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Importing blocks from disk...</source>
        <translation>S&apos;estan important els blocs del disc...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>S&apos;estan reindexant els blocs al disc...</translation>
    </message>
    <message>
        <location line="-554"/>
        <source>Send coins to a Nexa address</source>
        <translation>Envia monedes a una adreça Nexa</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>Realitza una còpia de seguretat del moneder a una altra ubicació</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Canvia la contrasenya d&apos;encriptació del moneder</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>&amp;Finestra de depuració</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Obre la consola de diagnòstic i depuració</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;Verifica el missatge...</translation>
    </message>
    <message>
        <location line="+522"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>Moneder</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>&amp;Envia</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;Rep</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Mostra / Amaga</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Mostra o amaga la finestra principal</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Encripta les claus privades pertanyents al moneder</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Signa el missatges amb la seva adreça de Nexa per provar que les poseeixes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Verifiqueu els missatges per assegurar-vos que han estat signats amb una adreça Nexa específica.</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>&amp;File</source>
        <translation>&amp;Fitxer</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuració</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajuda</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Barra d&apos;eines de les pestanyes</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Mostra la llista d&apos;adreces d&apos;enviament i etiquetes utilitzades</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Mostra la llista d&apos;adreces de recepció i etiquetes utilitzades</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>Opcions de la &amp;línia d&apos;ordres</translation>
    </message>
    <message numerus="yes">
        <location line="+390"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n connexió activa a la xarxa Nexa</numerusform>
            <numerusform>%n connexions actives a la xarxa Nexa</numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>No hi ha cap font de bloc disponible...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>S&apos;han processat %n bloc de l&apos;historial de transacció.</numerusform>
            <numerusform>S&apos;han processat %n blocs de l&apos;historial de transacció.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 behind</source>
        <translation>%1 darrere</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>El darrer bloc rebut ha estat generat fa %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Les transaccions a partir d&apos;això no seran visibles.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Avís</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Informació</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Up to date</source>
        <translation>Al dia</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Open a %1: URI or payment request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+194"/>
        <source>%1 client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Catching up...</source>
        <translation>S&apos;està posant al dia ...</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Date: %1
</source>
        <translation>Data: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Import: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Tipus: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Etiqueta: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Adreça: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Transacció enviada</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Transacció entrant</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>El moneder està &lt;b&gt;encriptat&lt;/b&gt; i actualment &lt;b&gt;desbloquejat&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>El moneder està &lt;b&gt;encriptat&lt;/b&gt; i actualment &lt;b&gt;bloquejat&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Selecció de moneda</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Quantitat:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Bytes:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Import:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Prioritat:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Comissió</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Polsim:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Comissió posterior:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Canvi:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>(des)selecciona-ho tot</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Mode arbre</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>Mode llista</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Import</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Rebut amb l&apos;etiqueta</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Rebut amb l&apos;adreça</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Confirmacions</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Confirmat</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Prioritat</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Copiar adreça </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copiar etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Copia l&apos;import</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Copiar ID de transacció</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>Bloqueja sense gastar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>Desbloqueja sense gastar</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Copia la quantitat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Copia la comissió</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Copia la comissió posterior</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Copia els bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Copia la prioritat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Copia el polsim</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Copia el canvi</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>El més alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>Més alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>mig-alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>mig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>baix-mig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>baix</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>més baix</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>el més baix</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 bloquejada)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>cap</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Aquesta etiqueta es torna en vermell si la transacció és superior a 1000 bytes.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Aquesta etiqueta es torna en vermell si la propietat és inferior que la «mitjana».</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Aquesta etiqueta es torna vermella si el destinatari rep un import inferior de %1.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Pot variar +/- %1 satoshi(s) per entrada.</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>yes</source>
        <translation>sí</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>Això comporta una comissió d&apos;almenys %1 per kB.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Pot variar +/- 1 byte per entrada.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Les transaccions amb una major prioritat són més propenses a ser incloses en un bloc.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(sense etiqueta)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>canvia de %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(canvia)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Edita l&apos;adreça</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>&amp;Etiqueta</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>L&apos;etiqueta associada amb aquesta entrada de llista d&apos;adreces</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>L&apos;adreça associada amb aquesta entrada de llista d&apos;adreces. Només es pot modificar per a les adreces d&apos;enviament.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>&amp;Adreça</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Nova adreça de recepció.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Nova adreça d&apos;enviament</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Edita les adreces de recepció</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Edita les adreces d&apos;enviament</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>L&apos;adreça introduïda «%1» ja és present a la llibreta d&apos;adreces.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>L&apos;adreça introduïda «%1» no és una adreça de Nexa vàlida.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>No s&apos;ha pogut desbloquejar el moneder.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Ha fallat la generació d&apos;una nova clau.</translation>
    </message>
</context>
<context>
    <name>EditTokenDialog</name>
    <message>
        <location filename="../forms/edittokendialog.ui" line="+14"/>
        <source>Edit Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Group ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The group ID of the token to be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../edittokendialog.cpp" line="+25"/>
        <source>Add new token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove existing token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Token Dialog Error! Please notify a developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+20"/>
        <source>Group ID required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Failed to add token: Invalid Group ID.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to add token.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Failed to remove token.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>Es crearà un nou directori de dades.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>El directori ja existeix. Afegeix %1 si vols crear un nou directori en aquesta ubicació.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>El camí ja existeix i no és cap directori.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>No es pot crear el directori de dades aquí.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>versió</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-bit)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Opcions de línia d&apos;ordres</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Ús:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>Opcions de la línia d&apos;ordres</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Us donem la benviguda</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Utilitza el directori de dades per defecte</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Utilitza un directori de dades personalitzat:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Error: el directori de dades «%1» especificat no pot ser creat.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GB d&apos;espai lliure disponible</numerusform>
            <numerusform>%n GB d&apos;espai lliure disponible</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(de %n GB necessari)</numerusform>
            <numerusform>(de %n GB necessaris)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">Formulari</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation type="unfinished">Últim temps de bloc</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation type="unfinished">Amaga</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>Obre un URI</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Obre una sol·licitud de pagament des d&apos;un URI o un fitxer</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Selecciona un fitxer de sol·licitud de pagament</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Selecciona el fitxer de sol·licitud de pagament per obrir</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Opcions</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;Principal</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Nombre de fils de &amp;verificació d&apos;scripts</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Allow incoming connections</source>
        <translation>Permet connexions entrants</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>Adreça IP del proxy (p. ex. IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Minimitza en comptes de sortir de l&apos;aplicació quan la finestra es tanca. Quan s&apos;habilita aquesta opció l&apos;aplicació es tancara només quan se selecciona Surt del menú. </translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>URL de terceres parts (p. ex. explorador de blocs) que apareix en la pestanya de transaccions com elements del menú contextual. %s en l&apos;URL es reemplaçat pel resum de la transacció. Diferents URL estan separades per una barra vertical |.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>URL de transaccions de terceres parts</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Opcions de línies d&apos;ordre active que sobreescriuen les opcions de dalt:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Reestableix totes les opcions del client.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Reestableix les opcions</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>&amp;Xarxa</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = auto, &lt;0 = deixa tants nuclis lliures)</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>W&amp;allet</source>
        <translation>&amp;Moneder</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Expert</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>Activa les funcions de &amp;control de les monedes</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Si inhabiliteu la despesa d&apos;un canvi sense confirmar, el canvi d&apos;una transacció no pot ser utilitzat fins que la transacció no tingui com a mínim una confirmació. Això també afecta com es calcula el vostre balanç.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Gasta el canvi sense confirmar</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Token Whitelist is enabled you can control what tokens appear in your wallet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Token Whitelist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Obre el port del client de Nexa al router de forma automàtica. Això només funciona quan el router implementa UPnP i l&apos;opció està activada.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Port obert amb &amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Connecta a la xarxa Nexa a través d&apos;un proxy SOCKS5.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>&amp;Connecta a través d&apos;un proxy SOCKS5 (proxy per defecte):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>&amp;IP del proxy:</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Port del proxy (per exemple 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Utilitzat per arribar als iguals mitjançant:</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Utilitza un proxy SOCKS4 apart per a arribar als iguals a través de serveis ocults de Tor:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;Finestra</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Mostra només la icona de la barra en minimitzar la finestra.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;Minimitza a la barra d&apos;aplicacions en comptes de la barra de tasques</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>M&amp;inimitza en tancar</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>&amp;Pantalla</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>&amp;Llengua de la interfície d&apos;usuari:</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>&amp;Unitats per mostrar els imports en:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Selecciona la unitat de subdivisió per defecte per mostrar en la interfície quan s&apos;envien monedes.</translation>
    </message>
    <message>
        <location line="-518"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Si voleu mostrar les funcions de control de monedes o no.</translation>
    </message>
    <message>
        <location line="-137"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+365"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-36"/>
        <source>IPv4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+166"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+186"/>
        <source>&amp;OK</source>
        <translation>&amp;D&apos;acord</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·la</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+135"/>
        <source>default</source>
        <translation>Per defecte</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>cap</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Confirm options reset</source>
        <translation>Confirmeu el reestabliment de les opcions</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Cal reiniciar el client per activar els canvis.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>S&apos;aturarà el client. Voleu procedir?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Amb aquest canvi cal un reinici del client.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>L&apos;adreça proxy introduïda és invalida.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>La informació mostrada pot no estar al día. El teu moneder es sincronitza automàticament amb la xarxa Nexa un cop s&apos;ha establert connexió, però aquest proces no s&apos;ha completat encara.</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>Només lectura:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Disponible:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>El balanç que podeu gastar actualment</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>Pendent:</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Total de transaccions que encara han de confirmar-se i que encara no compten en el balanç que es pot gastar</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>Immadur:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Balanç minat que encara no ha madurat</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>Balances</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>Total:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>El balanç total actual</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>El vostre balanç actual en adreces de només lectura</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>Que es pot gastar:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Transaccions recents</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Transaccions sense confirmar a adreces de només lectura</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Balanç minat en adreces de només lectura que encara no ha madurat</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Balanç total actual en adreces de només lectura</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>Gestió d&apos;URI</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>Adreça de pagament no vàlida %1</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>La sol·licitud de pagament s&apos;ha rebutjat</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>La xarxa de la sol·licitud de pagament no coincideix amb la xarxa del client.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Payment request is not initialized.</source>
        <translation>La sol·licitud de pagament no està inicialitzada.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>L&apos;import de pagament sol·licitat %1 és massa petit (es considera polsim).</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>Error en la sol·licitud de pagament</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation>No es pot iniciar gestor clica-per-pagar</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>L&apos;URL de recuperació de la sol·licitud de pagament no és vàlida: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>L&apos;URI no pot ser analitzat! Això pot ser a causa d&apos;una adreça de Nexa no vàlida o per paràmetres URI amb mal format.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>Gestió de fitxers de les sol·licituds de pagament</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>No es pot llegir el fitxer de la sol·licitud de pagament. Això pot ser causat per un fitxer de sol·licitud de pagament no vàlid.</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Payment request expired.</source>
        <translation>La sol·licitud de pagament ha vençut.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>No s&apos;accepten sol·licituds de pagament no verificades a scripts de pagament personalitzats.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>Sol·licitud de pagament no vàlida.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>Reemborsament de %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>La sol·licitud de pagament %1 és massa gran (%2 bytes, permès %3 bytes).</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>Error en comunicar amb %1: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>No es pot analitzar la sol·licitud de pagament!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>Mala resposta del servidor %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>Pagament reconegut</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>Error en la sol·licitud de xarxa</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+120"/>
        <source>User Agent</source>
        <translation>Agent d&apos;usuari</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>Node/Servei</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Temps de ping</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+831"/>
        <source>%1 d</source>
        <translation>%1 d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Cap</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation type="unfinished">
            <numerusform>%n hora</numerusform>
            <numerusform>%n hores</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation type="unfinished">
            <numerusform>%n dia</numerusform>
            <numerusform>%n dies</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation type="unfinished">
            <numerusform>%n setmana</numerusform>
            <numerusform>%n setmanes</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation type="unfinished">%1 i %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation type="unfinished">
            <numerusform>%n any</numerusform>
            <numerusform>%n anys</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>De&amp;sa la imatge...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>&amp;Copia la imatge</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>Desa el codi QR</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>Imatge PNG (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>Versió del client</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;Informació</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>Finestra de depuració</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>Utilitzant BerkeleyDB versió</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Startup time</source>
        <translation>&amp;Temps d&apos;inici</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>Xarxa</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Nombre de connexions</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>Cadena de blocs</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>Nombre de blocs actuals</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Rebut</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Enviat</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>&amp;Iguals</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Iguals bandejats</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Seleccioneu un igual per mostrar informació detallada.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>A la llista blanca</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Direcció</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Versió</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Starting Block</source>
        <translation>Bloc d&apos;inici</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>Capçaleres sincronitzades</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Blocs sincronitzats</translation>
    </message>
    <message>
        <location line="-2488"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>Agent d&apos;usuari</translation>
    </message>
    <message>
        <location line="-2370"/>
        <source>Datadir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Last block time (time since)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+769"/>
        <source>Services</source>
        <translation>Serveis</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Ban Score</source>
        <translation>Puntuació de bandeig</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Temps de connexió</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Darrer enviament</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Darrera recepció</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Temps de ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>La duració d&apos;un ping més destacat actualment.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Espera de ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Diferència horària</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>&amp;Obre</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>&amp;Consola</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>Trà&amp;nsit de la xarxa</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>Nete&amp;ja</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Totals</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>Dins:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Fora:</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>Fitxer de registre de depuració</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>Neteja la consola</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation>&amp;Desconnecta el node</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Bandeja el node durant</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 &amp;hora</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 &amp;dia</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 &amp;setmana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 &amp;any</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>&amp;Desbandeja el node</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Utilitza les fletxes d&apos;amunt i avall per navegar per l&apos;historial, i &lt;b&gt;Ctrl-L&lt;\b&gt; per netejar la pantalla.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Escriviu &lt;b&gt;help&lt;\b&gt; per a obtenir un llistat de les ordres disponibles.</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(id del node: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>a través de %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>mai</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Entrant</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Sortint</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Desconegut</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+166"/>
        <source>&amp;Amount:</source>
        <translation>Im&amp;port:</translation>
    </message>
    <message>
        <location line="-113"/>
        <source>&amp;Label:</source>
        <translation>&amp;Etiqueta:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>&amp;Message:</source>
        <translation>&amp;Missatge:</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Reutilitza una de les adreces de recepció utilitzades anteriorment. La reutilització d&apos;adreces pot comportar problemes de seguretat i privadesa. No ho utilitzeu llevat que torneu a generar una sol·licitud de pagament feta abans.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>R&amp;eutilitza una adreça de recepció anterior (no recomanat)</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+110"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>Un missatge opcional que s&apos;adjuntarà a la sol·licitud de pagament, que es mostrarà quan s&apos;obri la sol·licitud. Nota: El missatge no s&apos;enviarà amb el pagament per la xarxa Nexa.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+106"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Una etiqueta opcional que s&apos;associarà amb la nova adreça receptora.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Utilitzeu aquest formulari per sol·licitar pagaments. Tots els camps són &lt;b&gt;opcionals&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+120"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Un import opcional per sol·licitar. Deixeu-ho en blanc o zero per no sol·licitar cap import específic.</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>Clear all fields of the form.</source>
        <translation>Esborra tots els camps del formuari.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Neteja</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Requested payments history</source>
        <translation>Historial de pagaments sol·licitats</translation>
    </message>
    <message>
        <location line="-132"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Sol·licitud de pagament</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Mostra la sol·licitud seleccionada (fa el mateix que el doble clic a una entrada)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Esborra les entrades seleccionades de la llista</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Esborra</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+50"/>
        <source>Copy URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copia l&apos;etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Copia el missatge</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copia l&apos;import</translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>Codi QR</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>Copia l&apos;&amp;URI</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>Copia l&apos;&amp;adreça</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>De&amp;sa la imatge...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Sol·licita un pagament a %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Informació de pagament</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Import</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Missatge</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>URI resultant massa llarga, intenta reduir el text per a la etiqueta / missatge</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Error en codificar l&apos;URI en un codi QR.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Missatge</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>Import</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(sense etiqueta)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(sense missatge)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(sense import)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+626"/>
        <source>Send Coins</source>
        <translation>Envia monedes</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Característiques de control de les monedes</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>Entrades...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>seleccionat automàticament</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Fons insuficients!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Quantitat:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Bytes:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Import:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Prioritat:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Comissió:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>Comissió posterior:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Canvi:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Si s&apos;activa això, però l&apos;adreça de canvi està buida o bé no és vàlida, el canvi s&apos;enviarà a una adreça generada de nou.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>Personalitza l&apos;adreça de canvi</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>Comissió de transacció</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>Tria...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>redueix els paràmetres de comissió</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>per kilobyte</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Si la comissió personalitzada es defineix a 1000 satoshis i la transacció és de només 250 bytes, llavors «per kilobyte» només es paguen 250 satoshis en una comissió, mentre que amb la de «total com a mínim» es pagarien 1000 satoshis. Per a transaccions superiors al kilobyte, en tots dos casos es paga per kilobyte.</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>Amaga</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>total com a mínim</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>No hi ha cap problema en pagar només la comissió mínima sempre que hi hagi menys volum de transacció que espai en els blocs. Però tingueu present que això pot acabar en una transacció que mai es confirmi una vegada hi hagi més demanda de transaccions de nexa que la xarxa pugui processar.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(llegiu l&apos;indicador de funció)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>Recomanada:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>Personalitzada:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(No s&apos;ha inicialitzat encara la comissió intel·ligent. Normalment pren uns pocs blocs...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation>Temps de confirmació:</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>ràpid</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Envia com a transacció de comissió zero si és possible</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(la confirmació pot trigar més temps)</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>Envia a múltiples destinataris al mateix temps</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Afegeix &amp;destinatari</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>Netejar tots els camps del formulari.</translation>
    </message>
    <message>
        <location line="-880"/>
        <source>Dust:</source>
        <translation>Polsim:</translation>
    </message>
    <message>
        <location line="+883"/>
        <source>Clear &amp;All</source>
        <translation>Neteja-ho &amp;tot</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Current Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>Balanç:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Confirm the send action</source>
        <translation>Confirma l&apos;acció d&apos;enviament</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>E&amp;nvia</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>Confirma l&apos;enviament de monedes</translation>
    </message>
    <message>
        <location line="-52"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 a %2</translation>
    </message>
    <message>
        <location line="-289"/>
        <source>Copy quantity</source>
        <translation>Copia la quantitat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copia l&apos;import</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Copia la comissió</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Copia la comissió posterior</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Copia els bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Copia la prioritat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy change</source>
        <translation>Copia el canvi</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Total Amount %1</source>
        <translation>Import total %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>o</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>L&apos;import a pagar ha de ser major que 0.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>L&apos;import supera el vostre balanç.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>El total excedeix el teu balanç quan s&apos;afegeix la comissió a la transacció %1.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction creation failed!</source>
        <translation>Ha fallat la creació de la transacció!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>S&apos;ha rebutjat la transacció! Això pot passar si alguna de les monedes del vostre moneder ja s&apos;han gastat; per exemple, si heu fet servir una còpia de seguretat del fitxer wallet.dat i s&apos;haguessin gastat monedes de la còpia però sense marcar-les-hi com a gastades.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Una comissió superior a %1 es considera una comissió absurdament alta.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>La sol·licitud de pagament ha vençut.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>Paga només la comissió necessària de %1</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>L&apos;adreça de destinatari no és vàlida. Torneu-la a comprovar.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>S&apos;ha trobat una adreça duplicada: cal utilitzar les adreces només un cop cada vegada.</translation>
    </message>
    <message>
        <location line="+252"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Avís: adreça Nexa no vàlida</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(sense etiqueta)</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>Avís: adreça de canvi desconeguda</translation>
    </message>
    <message>
        <location line="-777"/>
        <source>Copy dust</source>
        <translation>Copia el polsim</translation>
    </message>
    <message>
        <location line="+291"/>
        <source>Are you sure you want to send?</source>
        <translation>Esteu segur que ho voleu enviar?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>S&apos;ha afegit una taxa de transacció</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+86"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>Q&amp;uantitat:</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>Paga &amp;a:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>Escull una adreça feta servir anteriorment</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Private Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>L&apos;adreça Nexa on enviar el pagament</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alta+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Enganxar adreça del porta-retalls</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>Elimina aquesta entrada</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>La comissió es deduirà de l&apos;import que s&apos;enviarà. El destinatari rebrà menys coins que les que introduïu al camp d&apos;import. Si se seleccionen múltiples destinataris, la comissió es dividirà per igual.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Amount to send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>S&amp;ubstreu la comissió de l&apos;import</translation>
    </message>
    <message>
        <location line="+643"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Aquesta és una sol·licitud de pagament no autenticada.</translation>
    </message>
    <message>
        <location line="+556"/>
        <source>This is an authenticated payment request.</source>
        <translation>Aquesta és una sol·licitud de pagament autenticada.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Un missatge que s&apos;ha adjuntat al %1 URI que s&apos;emmagatzemarà amb la transacció per a la vostra referència. Nota: el missatge no s&apos;enviarà a través de la xarxa Nexa.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-541"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>Paga a:</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>Memo:</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>No apagueu l&apos;ordinador fins que no desaparegui aquesta finestra.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Signatures - Signa / verifica un missatge</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>&amp;Signa el missatge</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Podeu signar missatges/acords amb les vostres adreces per provar que rebeu les coins que s&apos;hi envien. Aneu amb compte no signar res que sigui vague o aleatori, perquè en alguns atacs de suplantació es pot provar que hi signeu la vostra identitat. Només signeu aquelles declaracions completament detallades en què hi esteu d&apos;acord. </translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>L&apos;adreça Nexa amb què signar el missatge</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Tria les adreces fetes servir amb anterioritat</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Enganxa l&apos;adreça del porta-retalls</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Introduïu aquí el missatge que voleu signar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Signatura</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Copia la signatura actual al porta-retalls del sistema</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Signa el missatge per provar que ets propietari d&apos;aquesta adreça Nexa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>Signa el &amp;missatge</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Neteja tots els camps de clau</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>Neteja-ho &amp;tot</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>&amp;Verifica el missatge</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Introduïu l&apos;adreça del receptor, el missatge (assegureu-vos de copiar els salts de línia, espais, tabuladors, etc. exactament) i signatura de sota per verificar el missatge. Tingueu cura de no llegir més en la signatura del que està al missatge signat, per evitar ser enganyat per un atac d&apos;home-en-el-mig. Tingueu en compte que això només demostra que la part que signa rep amb l&apos;adreça, i no es pot provar l&apos;enviament de qualsevol transacció!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>L&apos;adreça Nexa amb què va ser signat el missatge</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Verificar el missatge per assegurar-se que ha estat signat amb una adreça Nexa específica</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>Verifica el &amp;missatge</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Neteja tots els camps de verificació de missatge</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Feu clic a «Signa el missatge» per a generar una signatura</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>El desbloqueig del moneder ha estat cancelat.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Missatge signat.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Missatge verificat.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TokenDisplayDialog</name>
    <message>
        <location filename="../forms/tokendisplaydialog.ui" line="+14"/>
        <source>Token Display Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a token to the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add New Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove a token from the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Group ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tokendisplaydialog.cpp" line="+43"/>
        <source>Copy GroupID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Token Ticker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation type="unfinished">Tot</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation type="unfinished">Avui</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation type="unfinished">Aquesta setmana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation type="unfinished">Aquest mes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation type="unfinished">El mes passat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation type="unfinished">Enguany</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation type="unfinished">Rang...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation type="unfinished">Rebut amb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation type="unfinished">Enviat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation type="unfinished">A un mateix</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation type="unfinished">Altres</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation type="unfinished">Import mínim</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished">Copia l&apos;import</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation type="unfinished">Mostra detalls de la transacció</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>Export Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation type="unfinished">Confirmat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation type="unfinished">Només de lectura</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation type="unfinished">Data</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation type="unfinished">Tipus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation type="unfinished">Etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation type="unfinished">Adreça</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation type="unfinished">L&apos;exportació ha fallat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation type="unfinished">Exportació amb èxit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Range:</source>
        <translation type="unfinished">Rang:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation type="unfinished">a</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+354"/>
        <source>Date</source>
        <translation type="unfinished">Data</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation type="unfinished">Tipus</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished">
            <numerusform>Obre per %n bloc més</numerusform>
            <numerusform>Obre per %n blocs més</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation type="unfinished">Obert fins %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation type="unfinished">Fora de línia</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation type="unfinished">Sense confirmar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation type="unfinished">Confirmant (%1 de %2 confirmacions recomanades)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation type="unfinished">Confirmat (%1 confirmacions)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation type="unfinished">En conflicte</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation type="unfinished">Immadur (%1 confirmacions, serà disponible després de %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation type="unfinished">Aquest bloc no ha estat rebut per cap altre node i probablement no serà acceptat!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation type="unfinished">Generat però no acceptat</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation type="unfinished">Rebut amb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation type="unfinished">Rebut de</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation type="unfinished">Enviat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation type="unfinished">Pagament a un mateix</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation type="unfinished">Altres</translation>
    </message>
    <message>
        <location line="+325"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation type="unfinished">Estat de la transacció. Desplaceu-vos sobre aquest camp per mostrar el nombre de confirmacions.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation type="unfinished">Data i hora en que la transacció va ser rebuda.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation type="unfinished">Tipus de transacció.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation type="unfinished">Si està implicada o no una adreça només de lectura en la transacció.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation type="unfinished">Intenció/propòsit de la transacció definida per l&apos;usuari.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation type="unfinished">Import extret o afegit del balanç.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable which tokens in the wallet are to be displayed in QT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Manage Displayed Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens in Wallet:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens Displayed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Amount: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation type="unfinished">L&apos;adreça Nexa on enviar el pagament</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation type="unfinished">Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation type="unfinished">Elimina aquesta entrada</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation type="unfinished">Nom</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+397"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation type="unfinished">Balanç:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation type="unfinished">Pendent:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation type="unfinished">Esteu segur que ho voleu enviar?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+35"/>
        <source>Open until %1</source>
        <translation>Obert fins %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>en conflicte</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/fora de línia</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/sense confirmar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 confirmacions</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Estat</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, difusió a través de %n node</numerusform>
            <numerusform>, difusió a través de %n nodes</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Font</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Generat</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+122"/>
        <source>From</source>
        <translation>Des de</translation>
    </message>
    <message>
        <location line="-142"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+108"/>
        <source>To</source>
        <translation>A</translation>
    </message>
    <message>
        <location line="-209"/>
        <source>double spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+70"/>
        <source>change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>Adreça pròpia</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+113"/>
        <source>watch-only</source>
        <translation>només lectura</translation>
    </message>
    <message>
        <location line="-110"/>
        <location line="+28"/>
        <location line="+107"/>
        <source>label</source>
        <translation>etiqueta</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+57"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-149"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+256"/>
        <source>Credit</source>
        <translation>Crèdit</translation>
    </message>
    <message numerus="yes">
        <location line="-404"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>disponible en %n bloc més</numerusform>
            <numerusform>disponibles en %n blocs més</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>no acceptat</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+261"/>
        <source>Debit</source>
        <translation>Dèbit</translation>
    </message>
    <message>
        <location line="-280"/>
        <source>Total debit</source>
        <translation>Dèbit total</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Crèdit total</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Comissió de transacció</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Import net</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+13"/>
        <source>Message</source>
        <translation>Missatge</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Comment</source>
        <translation>Comentar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Mercader</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Les monedes generades han de madurar %1 blocs abans de poder ser gastades. Quan genereu aquest bloc, es farà saber a la xarxa per tal d&apos;afegir-lo a la cadena de blocs. Si no pot fer-se lloc a la cadena, el seu estat canviarà a «no acceptat» i no es podrà gastar. Això pot passar ocasionalment si un altre node genera un bloc en un marge de segons respecte al vostre.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Name</source>
        <translation type="unfinished">Nom</translation>
    </message>
    <message>
        <location line="-77"/>
        <location line="+32"/>
        <location line="+48"/>
        <source>Decimals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+4"/>
        <source>Melt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+4"/>
        <source>Amount Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Amount Received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Debug information</source>
        <translation>Informació de depuració</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transacció</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Entrades</translation>
    </message>
    <message>
        <location line="-133"/>
        <location line="+4"/>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <location line="+57"/>
        <source>Amount</source>
        <translation>Import</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>cert</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>fals</translation>
    </message>
    <message>
        <location line="-533"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, encara no ha estat emès correctement</translation>
    </message>
    <message numerus="yes">
        <location line="-40"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Obre per %n bloc més</numerusform>
            <numerusform>Obre per %n blocs més</numerusform>
        </translation>
    </message>
    <message>
        <location line="+73"/>
        <source>unknown</source>
        <translation>desconegut</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Detall de la transacció</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Aquest panell mostra una descripció detallada de la transacció</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Tipus</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Immadur (%1 confirmacions, serà disponible després de %2)</translation>
    </message>
    <message numerus="yes">
        <location line="-29"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Obre per %n bloc més</numerusform>
            <numerusform>Obre per %n blocs més</numerusform>
        </translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Address or Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>Obert fins %1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Confirmat (%1 confirmacions)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Aquest bloc no ha estat rebut per cap altre node i probablement no serà acceptat!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Generat però no acceptat</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>Fora de línia</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Sense confirmar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Confirmant (%1 de %2 confirmacions recomanades)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>En conflicte</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Rebut amb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Rebut de</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Enviat a</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Pagament a un mateix</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Minat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation type="unfinished">Altres</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>només lectura</translation>
    </message>
    <message>
        <location line="+277"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Estat de la transacció. Desplaceu-vos sobre aquest camp per mostrar el nombre de confirmacions.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Data i hora en que la transacció va ser rebuda.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Tipus de transacció.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Si està implicada o no una adreça només de lectura en la transacció.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Intenció/propòsit de la transacció definida per l&apos;usuari.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Import extret o afegit del balanç.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Tot</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Avui</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Aquesta setmana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Aquest mes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>El mes passat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Enguany</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Rang...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Rebut amb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Enviat a</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>A un mateix</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Minat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Altres</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Introduïu una adreça o una etiqueta per cercar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Import mínim</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Copia l&apos;adreça</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copiar etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copia l&apos;import</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit label</source>
        <translation>Editar etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Mostra detalls de la transacció</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Export Transaction History</source>
        <translation>Exporta l&apos;historial de transacció</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Watch-only</source>
        <translation>Només de lectura</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>L&apos;exportació ha fallat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>S&apos;ha produït un error en provar de desar l&apos;historial de transacció a %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Exportació amb èxit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>L&apos;historial de transaccions s&apos;ha desat correctament a %1.</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Fitxer separat per comes (*.csv)</translation>
    </message>
    <message>
        <location line="-179"/>
        <source>Copy transaction idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Confirmed</source>
        <translation>Confirmat</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Tipus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Range:</source>
        <translation>Rang:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>a</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Unitat en què mostrar els imports. Feu clic per seleccionar una altra unitat.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation type="unfinished">&amp;Xarxa</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation type="unfinished">Opcions de línies d&apos;ordre active que sobreescriuen les opcions de dalt:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation type="unfinished">Reestableix totes les opcions del client.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation type="unfinished">&amp;Reestableix les opcions</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;D&apos;acord</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Cancel·la</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation type="unfinished">Confirmeu el reestabliment de les opcions</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation type="unfinished">Cal reiniciar el client per activar els canvis.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation type="unfinished">S&apos;aturarà el client. Voleu procedir?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>No s&apos;ha carregat cap moneder.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+300"/>
        <source>Send Coins</source>
        <translation>Envia monedes</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>&amp;Exporta</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exporta les dades de la pestanya actual a un fitxer</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>Còpia de seguretat del moneder</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Dades del moneder (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>Ha fallat la còpia de seguretat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>S&apos;ha produït un error en provar de desar les dades del moneder a %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>S&apos;han desat les dades del moneder correctament a %1.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>La còpia de seguretat s&apos;ha realitzat correctament</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+28"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Distribuït sota llicència de programari MIT. Vegeu el fitxer acompanyant COPYING o &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Rescans are not possible in pruned mode. You will need to use -resync which will download the whole blockchain again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Aquesta és una versió de pre-llançament - utilitza-la sota la teva responsabilitat - No usar per a minería o aplicacions de compra-venda</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>AVÍS: s&apos;ha generat un nombre anòmalament alt de blocs, %d blocs rebuts en les darreres %d hores (se n&apos;esperaven %d)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>AVÍS: comproveu la vostra connexió a la xarxa, %d blocs rebuts en les darreres %d hores (se n&apos;esperaven %d)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Avís: la xarxa no sembla que hi estigui plenament d&apos;acord. Alguns miners sembla que estan experimentant problemes.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Avís: sembla que no estem plenament d&apos;acord amb els nostres iguals! Podria caler que actualitzar l&apos;aplicació, o potser que ho facin altres nodes.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Corrupted block database detected</source>
        <translation>S&apos;ha detectat una base de dades de blocs corrupta</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Voleu reconstruir la base de dades de blocs ara?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>Error carregant la base de dades de blocs</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Error inicialitzant l&apos;entorn de la base de dades del moneder %s!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>Error en obrir la base de dades de blocs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>Error: Espai al disc baix!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Ha fallat escoltar a qualsevol port. Feu servir -listen=0 si voleu fer això.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>S&apos;està important...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>No s&apos;ha trobat el bloc de gènesi o és incorrecte. El directori de dades de la xarxa és incorrecte?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Adreça -onion no vàlida: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>No hi ha suficient descriptors de fitxers disponibles.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>La poda no es pot configurar amb un valor negatiu.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>El mode de poda és incompatible amb -txindex.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Signing token transaction failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Verifying blocks...</source>
        <translation>S&apos;estan verificant els blocs...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>S&apos;està verificant el moneder...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>El moneder %s resideix fora del directori de dades %s</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Error: ha fallat escoltar les connexions entrants (l&apos;escoltament ha retornat l&apos;error %s)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>L&apos;import de la transacció és massa petit per enviar-la després que se&apos;n dedueixi la comissió</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Aquest producte inclou programari desenvolupat pel projecte OpenSSL per a ús a l&apos;OpenSSL Toolkit &lt;https://www.openssl.org/&gt; i programari criptogràfic escrit per Eric Young i programari UPnP escrit per Thomas Bernard.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Activating best chain...</source>
        <translation>S&apos;està activant la millor cadena...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>No es pot resoldre l&apos;adreça -whitebind: «%s»</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Copyright (C) 2015-%i Els desenvolupadors del Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Information</source>
        <translation>&amp;Informació</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>S&apos;ha especificat una màscara de xarxa no vàlida a -whitelist: «%s»</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Cal especificar un port amb -whitebind: «%s»</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Signing transaction failed</source>
        <translation>Ha fallat la signatura de la transacció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>L&apos;import de la transacció és massa petit per pagar-ne una comissió</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Això és programari experimental.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Import de la transacció massa petit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Els imports de les transaccions han de ser positius</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Transacció massa gran per a la política de comissions</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>No s&apos;ha pogut vincular a %s en aquest ordinador (la vinculació ha retornat l&apos;error %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Avís</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Loading addresses...</source>
        <translation>S&apos;estan carregant les adreces...</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Adreça -proxy invalida: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>Xarxa desconeguda especificada a -onlynet: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>No es pot resoldre l&apos;adreça -bind: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>No es pot resoldre l&apos;adreça -externalip: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Loading block index...</source>
        <translation>S&apos;està carregant l&apos;índex de blocs...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>S&apos;està carregant el moneder...</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Cannot downgrade wallet</source>
        <translation>No es pot reduir la versió del moneder</translation>
    </message>
    <message>
        <location line="-131"/>
        <source>Nexa</source>
        <translation type="unfinished">Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You need to rebuild the database using -resync to go back to unpruned mode.  This will redownload the entire blockchain.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write default address</source>
        <translation>No es pot escriure l&apos;adreça per defecte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Opening Block database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pruning blockstore...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>S&apos;està reescanejant...</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zapping all transactions from wallet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Done loading</source>
        <translation>Ha acabat la càrrega</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
</context>
</TS>
